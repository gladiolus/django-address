from setuptools import setup, find_packages

version = '0.1.0'

setup(name='django_address',
      version=version,
      description="FIAS for Django",
      long_description=open("README.rst").read(),
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Django",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Libraries :: Python Modules",
        ],
      keywords='',
      author='ADONWEB',
      author_email='panaetovaa@gmail.com',
      url='',
      license='BSD',
      namespace_packages=['address'],
      packages=find_packages(exclude=['ez_setup']),
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          # -*- Extra requirements: -*-
      ],
      entry_points="""
      # -*- Entry points: -*-
      """,
      )
