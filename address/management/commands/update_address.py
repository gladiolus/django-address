# encoding=utf-8

import os
import logging
import urllib
import tempfile
import glob
import shutil
import rarfile
import json

from optparse import make_option
from suds.client import Client
from django.core.management.base import BaseCommand, CommandError
from address.importers import Importer
from address.models import CurrentVersion

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Imports valid addresses of Russia'

    option_list = BaseCommand.option_list + (
        make_option(
            '--dir', "-d",
            action='store',
            dest='dirpath',
            default=None,
            help='Directory where *.dbf files are located'),

        make_option(
            '--from_version',
            action='store',
            dest='from_version',
            type='int',
            default=None,
            help='From what version update is required'),

        make_option(
            '--ver',
            action='store',
            dest='ver',
            default=None,
            help='Version of source files (is used only when dirpath is set)'),

        make_option(
            '--update', "-u",
            action='store_true',
            dest='update_mode',
            help='Addresses will be updated'),

        make_option(
            '--create', "-c",
            action='store_true',
            dest='create_mode',
            help='Current addresses will be deleted and recreated'),

        make_option(
            '--zones', "-z",
            action='store_true',
            dest='zones',
            default=False,
            help='Handle zones'),

        make_option(
            '--houses', "-b",
            action='store_true',
            dest='buildings',
            default=False,
            help='Handle houses'),

        make_option(
            '--region', "-r",
            action='store',
            dest='region',
            type='int',
            default=None,
            help='Region number'),

        make_option(
            '--url',
            action='store',
            dest='url',
            default="http://fias.nalog.ru/WebServices/Public/DownloadService.asmx?WSDL",
            help='Upload url'),

        make_option(
            '--tmp',
            action='store',
            dest='tmp',
            default=None,
            help='Directory of temporary files'), )

    def handle(self, *args, **options):

        update_mode = options['update_mode']
        create_mode = options['create_mode']
        if not (update_mode or create_mode):
            return

        handle_zones = options["zones"]
        handle_buildings = options["buildings"]
        handle_abbrevs = handle_zones
        if not (handle_zones or handle_buildings or handle_abbrevs):
            return

        region = options["region"]

        dirpath = options["dirpath"]
        version = options["ver"]
        from_version = options["from_version"]

        params = dict(
            handle_zones=handle_zones, handle_buildings=handle_buildings,
            handle_abbrevs=handle_abbrevs,
            region=region
        )
        importer = Importer()

        if dirpath:
            if not version:
                raise CommandError('Version of sourse files is not specified!')

            if create_mode:
                importer.create(dirpath, **params)
            else:
                importer.update(dirpath, **params)

            CurrentVersion(
                version_id=int(version),
                label=json.dumps(params)
            ).save()

        else:

            if create_mode:
                # take the latest source files and use them
                cur_version = None
            else:
                if from_version:
                    cur_version = from_version
                else:
                    qs = CurrentVersion.objects.only("version_id")\
                                               .order_by("-version_id")[:1]
                    if len(qs):
                        cur_version = qs[0]
                    else:
                        cur_version = None

            url = options["url"]
            client = Client(url)

            source = []
            if cur_version is None:
                # recreate objects using latest source files
                verinfo = client.service.GetLastDownloadFileInfo()
                source = [(
                    verinfo.FiasCompleteDbfUrl,
                    dict(id=verinfo.VersionId, text=verinfo.TextVersion),
                    False,
                )]
            else:
                verinfos = client.service.GetAllDownloadFileInfo()[0]
                source = []
                for verinfo in verinfos[1:]:
                    version_id = verinfo.VersionId
                    if cur_version < version_id:
                        source.append((
                            verinfo.FiasDeltaDbfUrl,
                            dict(id=verinfo.VersionId,
                                 text=verinfo.TextVersion),
                            True,
                        ))

            tmpdir = options["tmp"]
            if not tmpdir:
                tmpdir = tempfile.mkdtemp()
            else:
                tmpdir = tempfile.mkdtemp(dir=tmpdir)

            print tmpdir

            for db_url, ver, is_update in source:
                for fname in glob.glob(os.path.join(tmpdir, "*")):
                    os.remove(fname)

                archive_name = u"address.archive"
                archive_path = os.path.join(tmpdir, archive_name)
                urllib.urlretrieve(db_url, archive_path)

                rf = rarfile.RarFile(archive_path)
                if handle_zones:
                    rf.extract("ADDROBJ.DBF", path=tmpdir)

                if handle_abbrevs:
                    rf.extract("SOCRBASE.DBF", path=tmpdir)

                if handle_buildings:
                    if region:
                        hfname = "HOUSE%02d.DBF" % region
                    else:
                        hfname = "HOUSE*.DBF"
                    rf.extract(hfname, path=tmpdir)

                if is_update:
                    importer.update(tmpdir, **params)
                else:
                    importer.create(tmpdir, **params)

                CurrentVersion(
                    version_id=ver["id"],
                    label=json.dumps(params)
                ).save()

            shutil.rmtree(tmpdir)
