# encoding=utf-8

import dbf
import os.path
import glob
import datetime

from django.db import IntegrityError

from models import Abbrev, Zone, House

import logging

logger = logging.getLogger(__name__)


class ImportModes(object):

    CREATE = 0
    UPDATE = 1


class AddressCmds(object):

    CREATE = 0
    UPDATE = 1
    DELETE = 2
    IGNORE = 3


class ZoneStats(object):

    INITED = 1
    ADDED = 10
    CHANGED = 20
    GRCHANGED = 21
    DELETED = 30
    DELETED_CAUSE_PARENT_DELETED = 31
    UNION = 40
    PARENT_UNIONED = 41
    UNIONED_AND_DELETED = 42
    UNIONED_AND_CREATED = 43
    PARENT_CHANGED = 50
    PARENT_CHANGED_PARENT = 51
    SPLITED_AND_DELETED = 61
    SPLITED_AND_CREATED = 62
    RECREATED = 70


class Importer(object):

    def __init__(self, abbrev_fdbname='SOCRBASE.DBF',
                 addr_fdbname='ADDROBJ.DBF',
                 building_fdbname_prefix='HOUSE',
                 bulk_save_limit=10000):

        self.abbrev_fdbname = abbrev_fdbname
        self.addr_fdbname = addr_fdbname
        self.building_fdbname_prefix = building_fdbname_prefix

        self.bulk_save_limit = bulk_save_limit
        self.today = datetime.date.today()

    def update(self, dirpath,
               handle_zones=True, handle_buildings=False,
               handle_abbrevs=True, region=None):

        self.exec_cmd(dirpath, ImportModes.UPDATE, handle_zones,
                      handle_buildings, handle_abbrevs, region)

    def create(self, dirpath,
               handle_zones=True, handle_buildings=False,
               handle_abbrevs=True, region=None):

        self.exec_cmd(dirpath, ImportModes.CREATE, handle_zones,
                      handle_buildings, handle_abbrevs, region)

    def exec_cmd(self, dirpath, mode,
                 handle_zones=True, handle_buildings=False,
                 handle_abbrevs=True, region=None):

        self.empty_obj_buffer()

        # Abbrev
        if handle_abbrevs:
            self.update_abbrevs(dirpath, mode)

        # Zone
        if handle_zones:
            self.update_addrobjs(dirpath, mode)

        # House
        if handle_buildings:
            self.update_buildings(dirpath, mode, region=region)

    def update_abbrevs(self, dirpath, mode):

        mode_create = mode == ImportModes.CREATE
        mode_update = mode == ImportModes.UPDATE

        if mode_create:
            Abbrev.objects.all().delete()

        with dbf.Table(os.path.join(dirpath, self.abbrev_fdbname)) as socrbase:
            socrbase.codepage = dbf.CodePage('cp866')

            for r in socrbase:

                level = r.level.strip()
                short = r.scname.strip()
                full = r.socrname.strip()

                if mode_create:
                    self.save_obj(Abbrev(
                        level=level,
                        short=short,
                        full=full,
                    ), Abbrev)
                elif mode_update:
                    abbrevs = list(Abbrev.objects.only("full")
                                                 .filter(level=level)
                                                 .filter(short=short)[:1])
                    if abbrevs:
                        abbrev = abbrevs[0]
                        if abbrev.full != full:
                            Abbrev.objects.filter(level=level)\
                                          .filter(short=short)\
                                          .update(full=full)
                    else:
                        self.save_obj(Abbrev(
                            level=level,
                            short=short,
                            full=full,
                        ), Abbrev)

        self.flush(Abbrev)

    def update_addrobjs(self, dirpath, mode):

        self.abbrev_cache = {}
        for abbrev in Abbrev.objects.all():
            self.abbrev_cache[(abbrev.level, abbrev.short)] = abbrev

        mode_create = mode == ImportModes.CREATE
        mode_update = mode == ImportModes.UPDATE

        if mode_create:
            Zone.objects.all().delete()

        with dbf.Table(os.path.join(dirpath, self.addr_fdbname)) as t:

            t.codepage = dbf.CodePage('cp866')

            for r in t:

                status = r.actstatus

                command = AddressCmds.IGNORE
                if mode_create:
                    # if we are creating a new base,
                    # only actual objects should be accepted
                    if status == 1:
                        command = AddressCmds.CREATE

                elif mode_update:
                    operstatus = r.operstatus

                    if operstatus in (ZoneStats.INITED,
                                      ZoneStats.ADDED,
                                      ZoneStats.UNIONED_AND_CREATED,
                                      ZoneStats.SPLITED_AND_CREATED,
                                      ZoneStats.RECREATED,):

                        command = AddressCmds.CREATE

                    elif operstatus in (
                            ZoneStats.CHANGED,
                            ZoneStats.GRCHANGED,
                            ZoneStats.UNION,
                            ZoneStats.PARENT_UNIONED,
                            ZoneStats.PARENT_CHANGED,
                            ZoneStats.PARENT_CHANGED_PARENT,):

                        command = AddressCmds.UPDATE

                    elif operstatus in (
                            ZoneStats.DELETED,
                            ZoneStats.DELETED_CAUSE_PARENT_DELETED,
                            ZoneStats.UNIONED_AND_DELETED,
                            ZoneStats.SPLITED_AND_DELETED,):

                        command = AddressCmds.DELETE

                if command == AddressCmds.IGNORE:
                    continue

                aoguid = r.aoguid.strip()
                if command == AddressCmds.DELETE:
                    Zone.objects.filter(guid=aoguid).update(deleted=True)

                else:
                    level = int(r.aolevel)
                    code = r.plaincode.strip()
                    zip_code = r.postalcode.strip()
                    offname = r.offname.strip()
                    short = r.shortname.strip()

                    # code is not set! That is must ne mistake.
                    if not code:
                        continue

                    params = dict(
                        name=offname,
                        short=short,
                        zip_code=zip_code,
                        kladr_code=code,
                        level=level,
                        parentguid=r.parentguid.strip(),
                        abbrev=self.abbrev_cache[(level, short)]
                    )

                    if command == AddressCmds.CREATE:
                        self.save_obj(Zone(guid=aoguid, **params), Zone)

                    elif command == AddressCmds.UPDATE:
                        Zone.objects.filter(guid=aoguid).update(**params)

        self.flush(Zone)

    def update_buildings(self, dirpath, mode, region=None):

        pref = self.building_fdbname_prefix

        if region:
            fname_template = pref + ("%02d" % region) + "*"
        else:
            fname_template = pref + "*"

        for fpath in glob.iglob(os.path.join(dirpath, fname_template)):
            self.parse_house(fpath, mode)

    def parse_house(self, fname, mode):

        # print fname
        fdbname = os.path.basename(fname)
        pref = self.building_fdbname_prefix
        try:
            region = int(fdbname[len(pref):-4], 10)
        except ValueError:
            # sorry! wrong file!
            return

        if mode == ImportModes.CREATE:
            qs = House.objects.filter(region=region)
            qs.delete()

        with dbf.Table(fname) as t:
            t.codepage = dbf.CodePage('cp866')

            for r in t:

                startdate = r.startdate
                enddate = r.enddate

                if mode == ImportModes.CREATE and (
                        self.today > enddate or self.today < startdate):
                    continue

                houseguid = r.houseguid.strip()
                params = dict(
                    zip_code=r.postalcode.strip(),
                    housenum=r.housenum.strip(),
                    buildnum=r.buildnum.strip(),
                    strucnum=r.strucnum.strip(),
                    guid=r.aoguid.strip(),
                    region=region,
                    enddate=enddate,
                )
                if mode == ImportModes.CREATE:
                    self.save_obj(
                        House(houseguid=houseguid, **params), House)

                elif mode == ImportModes.UPDATE:
                    qs = House.objects.filter(houseguid=houseguid)
                    if qs.exists():
                        qs.update(**params)
                    else:
                        self.save_obj(
                            House(houseguid=houseguid, **params), House)

        self.flush(House)

    def save_obj(self, obj, model):

        unsaved = self.unsaved_objs[model]
        unsaved.append(obj)
        if len(unsaved) > self.bulk_save_limit:
            self.flush(model)

    def flush(self, model):
        unsaved = self.unsaved_objs[model]
        objs_num = len(unsaved)

        if objs_num == 0:
            return

        start = 0
        finish = objs_num

        fs = [objs_num]
        while start < objs_num:

            try:
                model.objects.bulk_create(unsaved[start:finish])
            except IntegrityError as e:
                d = finish - start
                if d == 1:
                    # let's just miss this strange object and go on
                    logger.debug(
                        "Cannot save object of model %s: %s" %
                        (model, str(e)))
                    finish = fs.pop()
                    start += 1
                else:
                    fs.append(finish)
                    finish -= int(d / 2)

            else:
                start = finish
                finish = fs.pop()

        self.unsaved_objs[model] = []

    def empty_obj_buffer(self):

        self.unsaved_objs = {
            Zone: [],
            House: [],
            Abbrev: [],
        }
